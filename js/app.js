//Objetos en javascript
automovil = {
    marca: "carro",
    modelo: "yaris",
    descripcion: "carrazo bien rapidoteicial",
    imagen: "/img/img2.jpg"
}

//Mostar Objeto 

console.log("Automovil " + automovil);

console.log("Marca " + automovil.marca);
console.log("Modelo " + automovil.modelo);
console.log("Descripcion " + automovil.descripcion);
console.log("Imagen " + automovil.imagen);



// Arreglo de Objetos

automoviles= [{
    marca: "AUDI",
    modelo: "Audi R8",
    descripcion: "Audi R8 2024 Deportivo manual 2 puertas precio $200,000 dolares credito 20% pago inicial",
    imagen: "/img/img2.jpg"},
    {
    marca: "AUDI",
    modelo: "Audi Q3",
    descripcion: "Audi Q3 2024 Automatico 4 puertas precio $45,000 dolares credito 20% pago inicial",
    imagen: "/img/img3.jpg"
    },
    {
    marca: "AUDI",
    modelo: "Audi A3",
    descripcion: "Audi Q3 2024 Automatico 4 puertas precio $50,000 dolares credito 20% pago inicial",
    imagen: "/img/img4.jpg"
    },
    {
    marca: "MClaren",
    modelo: "MClaren 720s",
    descripcion: "MClaren 720s 2024 Deportivo automatico 2 puertas precio $300,000 dólares credito 20% pago inicial",
    imagen: "/img/img5.jpg"
    },
    {
    marca: "MClaren",
    modelo: "MClaren 765LT",
    descripcion: "Mclaren 765lt 2023 Deportivo automatico 2 puertas precio $350,000 dólares Credito 20% pago Inicial",
    imagen: "/img/img6.jpg"
    },
    {
    marca: "MClaren",
    modelo: "MClaren P1",
    descripcion: "MClaren P1 2015 deportivo 2 puertas precio $1.15 millones de dólares credito 20% pago inicial",
    imagen: "/img/img7.jpg"
    },
    {
    marca: "Ford",
    modelo: "Territory",
    descripcion: "Ford Territory 2024 Camioneta 4 puertas precio $597,000 de pesos credito 20% pago inicial",
    imagen: "/img/img8.jpg"
    },
    {
    marca: "Ford",
    modelo: "Bronco",
    descripcion: "Ford Bronco 2023 Camioneta todo terreno 4 puertas precio $732,000 de pesos credito 20% pago inicial",
    imagen: "/img/img9.jpg"
    },
    {
    marca: "Ford",
    modelo: "Edge",
    descripcion: "Ford Edge 2023 Camioneta 4 puertas precio $984,000 credito 20% pago inicial",
    imagen: "/img/img10.jpg"
    }];

    //Mostra los Objetos

    for(let con = 0; con<automoviles.length;++con){
        console.log("Marca " + automoviles[con].marca);
        console.log("Modelo " + automoviles[con].modelo);
        console.log("Descripcion " + automoviles[con].descripcion);
        console.log("Imagen " + automoviles[con].imagen);
    }

    const selMarc = document.getElementById("selMarc");
    selMarc.addEventListener('change', function cambio(){
    let opcion = parseInt(selMarc.value);

    let marcatitulo = document.getElementById("marcatitulo");
    marcatitulo.innerHTML = automoviles[opcion].marca;

    let modelo = document.getElementById("modelo");
    modelo.innerHTML= automoviles[opcion].modelo;

    const marca = document.getElementById("marca");
    marca.innerHTML= automoviles[opcion].marca;

    const descripcion = document.getElementById("descripcion");
    descripcion.innerHTML= automoviles[opcion].descripcion;

    const imagen = document.getElementById("imagen");
    imagen.src= automoviles[opcion].imagen;

    // Tarjeta 2
    if (automoviles[opcion + 1]) {
        let modelo1 = document.getElementById("modelo1");
        modelo1.innerHTML= automoviles[opcion+1].modelo;

        const marca1 = document.getElementById("marca1");
        marca1.innerHTML= automoviles[opcion+1].marca;

        const descripcion1 = document.getElementById("descripcion1");
        descripcion1.innerHTML= automoviles[opcion+1].descripcion;

        const imagen1 = document.getElementById("imagen1");
        imagen1.src= automoviles[opcion+1].imagen;
    }

    // Tarjeta 3
    if (automoviles[opcion + 2]) {
        let modelo2 = document.getElementById("modelo2");
        modelo2.innerHTML= automoviles[opcion+2].modelo;

        const marca2 = document.getElementById("marca2");
        marca2.innerHTML= automoviles[opcion+2].marca;

        const descripcion2 = document.getElementById("descripcion2");
        descripcion2.innerHTML= automoviles[opcion+2].descripcion;

        const imagen2 = document.getElementById("imagen2");
        imagen2.src= automoviles[opcion+2].imagen;
    }
});

